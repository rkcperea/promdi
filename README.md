# ProMDi
[ProMDi is a Filipino slang referring to people from the provinces or rural areas.](https://en.wikipedia.org/wiki/PROMDI)

## Name
ProMDi. 
- When Filipinos were asked where they were from? Answers usually sounds like, "I am PROMDI PROvince of..."
- Also known as the sexiest accent: Filipino English speakers' upper and lower lips touch after the count of three. wan tu tri por.

I am promdi province of Bulacan.

## Description
ProMDi or (initially) pre-MD *will be* a transpiler that *would* convert simpler notes into a better markdown file.
Where are Promdi MD files from? They are gnerated from the pre-MD promdi source codes.
#### Actors
Who will be using it? (i.e., the actors)
- primarily me, also, employers

#### Goals
What is their goal? What problem does this product or feature solve for them?
- [ ] speed up note taking
- [ ] DRY (anchor method: `hashy`)
- [ ] Track progress
- [ ] Add additional notes 
#### Workflows
What are some of the actions or operations they are likely to perform? (i.e., the workflows)
- [ ] Simple CRUD / or STD.IO
- [ ] parser
- [ ] `hashy`
- [ ] Upload
- [ ] Download

## Getting started
To make it easy for you to get started with GitLab, here's a list of recommended next steps.

<!-- ## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge. -->

<!-- ## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method. -->

## Installation
Install `ruby-3.2.1` on `rails 7.0.4.2`, 

## Usage
`bin/rails s`, create new PreMarkDown record or upload pre-MD file with file extension `.prmd`

## Support
rkcperea@gmail.com, +63 932 536 2109

## Roadmap
- For now, I'll use what I'm familiar with, rails. CRUD the pre-MD, and show action would be the MD.
- hashy method, anchor.
- Allow downloading
- Defaults to a single README.md, GitLab
- Change defaults: Github, multiple folders' README.md
- DelayedJob Commit to GitLab.

## Contributing
This is inspired by Svelte's real-time transpiler, right now, I can't do real-time but if you can kindly fork and send a pull request, or we can discuss so I can help out. 

## Authors and acknowledgment
Originally created for Rafael Perea's portfolio ([LinkedIn](https://www.linkedin.com/in/rafaelperea/), [Indeed](https://my.indeed.com/p/rafaelp-8pzwyrm), [GitHub](www.github.com/rkc), [GitLab](www.gitlab.com/rkc)) in order to speed up earning digital credentials ([credly](https://www.credly.com/users/rafael-perea.58243a30/badges)) in online courses: he needs to save time in taking notes by DRY-ing (Don't Repeat Yourself) with pre-MD.

And primarily, in church-gathering topic notes, too.

## License
Free for now.

## Project status
Planning: in progress
